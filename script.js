document.addEventListener('DOMContentLoaded', () => {
  let displayString = []
  let result = 0
  document.querySelector('.calc-display').innerText = 0
  function clickNum(event) {
    if (document.querySelector('.calc-display').innerText === '0') {
      document.querySelector('.calc-display').innerText = event.target.innerText
    } else {
      document.querySelector('.calc-display').innerText +=
        event.target.innerText
    }
  }

  function dotBtnFunc() {
    if (!document.querySelector('.calc-display').innerText.includes('.')) {
      document.querySelector('.calc-display').innerText += '.'
    }
  }

  function toggleOtherButtons() {
    document.querySelectorAll('.ar-btn').forEach((item) => {
      item.classList.toggle('unactive')
      item.removeEventListener('click', arFunction)
    })
    document.querySelectorAll('.calc-btn').forEach((item) => {
      if (item.innerText !== '=') {
        item.classList.toggle('unactive')
        item.removeEventListener('click', calcBtn)
      }
    })
  }

  function checkError() {
    if (
      Number(document.querySelector('.calc-display').innerText) === Infinity
    ) {
      document.querySelector('.calc-display').innerText = 'Error'
      setTimeout(() => {
        document.querySelector('.calc-display').innerText = 0
        document.querySelector('.secondary-display').innerText = null
        displayString = []
      }, 1000)
    }
  }
  function arFunction(event) {
    displayString.push(
      Number(document.querySelector('.calc-display').innerText)
    )
    document.querySelector('.calc-display').innerText = 0
    switch (event.target.innerText) {
      case '+':
        displayString.push('+')
        break
      case '-':
        displayString.push('-')
        break
      case '*':
        displayString.push('*')
        break
      case '/':
        displayString.push('/')
        break
      case '%':
        displayString.push('%')
        toggleOtherButtons()
        break
      default:
        break
    }
    document.querySelector('.secondary-display').innerText =
      displayString.join('')
  }
  function calcBtn(event) {
    displayString.push(
      Number(document.querySelector('.calc-display').innerText)
    )
    switch (event.target.innerText) {
      case '=':
        if (displayString[1] !== '%') {
          result = eval(displayString.join('')).toFixed(8)
        } else {
          toggleOtherButtons()
          result = (displayString[2] * 0.01 * displayString[0]).toFixed(8)
          document
            .querySelectorAll('.ar-btn')
            .forEach((item) => item.addEventListener('click', arFunction))
          document
            .querySelectorAll('.calc-btn')
            .forEach((item) => item.addEventListener('click', calcBtn))
        }
        document.querySelector('.calc-display').innerText = result
        document.querySelector('.secondary-display').innerText = null
        displayString = []
        checkError()
        break
      case 'square':
        document.querySelector('.calc-display').innerText =
          displayString[displayString.length - 1] ** 2
        break
      case 'sqroot':
        document.querySelector('.calc-display').innerText = Math.sqrt(
          displayString[displayString.length - 1]
        ).toFixed(8)
        break
      case 'Delete':
        displayString = []
        if (document.querySelector('.calc-display').innerText.length === 1) {
          document.querySelector('.calc-display').innerText = 0
        } else {
          document.querySelector('.calc-display').innerText = document
            .querySelector('.calc-display')
            .innerText.slice(0, -1)
        }
        break
      case 'C':
        displayString = []
        if (document.querySelector('.calc-display').innerText.length === 1) {
          document.querySelector('.calc-display').innerText = 0
        } else {
          document.querySelector('.calc-display').innerText = document
            .querySelector('.calc-display')
            .innerText.slice(0, -1)
        }
        break
      case 'CE':
        displayString = []
        document.querySelector('.calc-display').innerText = 0
        break
      case '1/x':
        document.querySelector('.calc-display').innerText =
          1 / displayString[displayString.length - 1].toFixed(8)
        checkError()
        break
      default:
        break
    }
  }

  function posNegFunction() {
    document.querySelector('.calc-display').innerText = -Number(
      document.querySelector('.calc-display').innerText
    )
  }

  document
    .querySelector('.pos-neg-btn')
    .addEventListener('click', posNegFunction)
  document
    .querySelectorAll('.ar-btn')
    .forEach((item) => item.addEventListener('click', arFunction))
  document
    .querySelectorAll('.calc-btn')
    .forEach((item) => item.addEventListener('click', calcBtn))
  document
    .querySelectorAll('.number-btn')
    .forEach((item) => item.addEventListener('click', clickNum))
  document.querySelector('.dot-btn').addEventListener('click', dotBtnFunc)
})
